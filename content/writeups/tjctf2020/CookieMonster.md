---
title: Cookie Monster
author: Brownie in Motion, arinerron
date: '2020-05-28'
ctfs:
  - tjctf2020
categories:
  - forensics
  - guess
---

# TJCTF 2020 - Cookie Monster

The flag existed in two parts in a massive 1 GB memory dump.

The challenge was honestly pretty cool. We had a ton of fun solving it. It was only slightly stressful because we knew b1c was on our tail.

Contrary to what you might think, Cookie Monster wasn’t very challenging. redpwn—in all of its creativity—came up with an even more fun challenge on top of Cookie Monster for itself.

## Part One — Reading the RDP Cache

Okay. What’s frustrating about this challenge for “me” (one of the redpwn people who worked on this challenge) is that this is one of the first places I checked for the flag.

However, due to my sheer incompetence and absolute lack of ability to do anything well, I ended up costing our team first place (note from editor: this isn’t true <3) and at least five hours of each of our team members’ time.

It is well-known that the Windows Remote Desktop application caches screenshots in bitmap files as the screen updates during a session ([writeup](https://cbtgeeks.com/2018/05/22/digital-forensics-on-rdp-cache/)).

So, after observing the path `Storage\Users\Administrator\AppData\Local\Microsoft\Terminal Server Client\Cache\` we immediately looked for `.bmc` files. We found them and extracted them using [volatility](https://github.com/volatilityfoundation/volatility):

```sh
mkdir -p /tmp/cache
for offset in $(volatility -f dump_patched.raw --cache --profile=Win7SP1x64 filescan | grep -i bmc | cut -d ':' -f 1); do
    volatility -f dump_patched.raw --cache --profile=Win7SP1x64 dumpfiles -Q “$offset” -D /tmp/cache
done
```

The first time our team member found these `.bmc` files, he used binwalk to try to extract the bitmap files. However, as both `binwalk` and the individual were equally as stupid, after `binwalk` found no no bitmap files, the team member quickly dismissed the idea that the flag would be in the cache—sending the rest of the team on an intense, dreadful wild goose chase that lasted five hours.

Once we finally got our act together, we extracted the bitmap files using a tool called [bmc-tools](https://github.com/ANSSI-FR/bmc-tools):

```sh
mkdir -p output

# python2 wtf
git clone https://github.com/ANSSI-FR/bmc-tools && cd bmc-tools
python2 bmc-tools.py -s ../file2.data -d output/
```

Finally, we’d found the bitmap files. We then spent twenty panicked minutes trying and failing to assemble them in an efficient way. The threat of b1c solving the challenge and pushing us down to 3rd place was ever-looming, and we were struggling to stay alive from staring at our screens for so long.

So, to assemble the images, we scrambled to do the following:

1. Open the images in a file manager, and fudge with the width of the window until the file previews appear to line up in a reasonable manner. Unfortunately, this task was only suited for the weak-minded Windows users and users of GUI file explorers like `nemo`. The TJCTF organizers intentionally created this challenge to disadvantage and discriminate against the CLI masterrace.

2. Take a screenshot and paste it into Microsoft Paint. Then, spend an ungodly amount of time removing the gutters between the thumbnails until you realize that something is wrong.

3. Because you solved part two first, you have a general idea of what the flag will look like. Now is a good time to begin guessing. You see that `c00kie` and `m0n5t3r` are in it, as well as `w4s` and `h3r3`. Just infer that they are joined with underscores, because that makes sense, right?

4. Get past Cloudflare’s stupid, custom captcha as you attempt to submit the flag. Though this may seem like an easy endeavor, the fact that companies now want to make captchas difficult for both people AND bots means that this will take much, much longer than you expect.

5. Submit the flag wrong many, many times. There are many ways to do this: copy it incorrectly, forget underscores, even accidentally replace some numbers with letters and vice versa. This is important in making the final moments of the CTF as stressful as possible.

6. Finally, submit it correctly. Take a sigh of relief as you get onto your feet: you will find yourself kneeling on the toilet on the urge of vomiting—if you’re lucky. The rest of the team (the CLI users) were already outside digging graves for themselves and getting ready to make their lives—or lack thereof—a whole lot better. 

Okay. I’m sorry, but we just can’t keep writing. Please excuse us while we step outside and try to forget this ever happened.

Whoops, we’re getting ahead of ourselves. Here’s how we got the second part of the flag:

## Part Two — Cloud Storage Account Takeover

We now need to travel back in time to a better point in our lives. One of hope, one of joy, one where the pain of looking through memory had barely grazed us. This is the part of the challenge we did first.

After running strings on the dump file and scrolling through it aimlessly for half an hour, we notice a number of POST requests to `http://super-secret-file-server.herokuapp.com/download`

```http
POST /download HTTP/1.1
Host: super-secret-file-server.herokuapp.com
Connection: keep-alive
Content-Length: 28
Cache-Control: max-age=0
Upgrade-Insecure-Requests: 1
Origin: http://super-secret-file-server.herokuapp.com
Content-Type: application/x-www-form-urlencoded
User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Referer: http://super-secret-file-server.herokuapp.com/
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Cookie: session=eyJsb2dnZWRfaW4iOnRydWV9.XsqeZQ.C7x84FROMlFfzg_2Wwaj0SDtNjY
```

With the cookie, we can get the second half of the flag:

```sh
curl http://super-secret-file-server.herokuapp.com/download -H 'Cookie: session=eyJsb2dnZWRfaW4iOnRydWV9.XsqeZQ.C7x84FROMlFfzg_2Wwaj0SDtNjY' -v --data 'filename=supersecretflag.txt&submit=Download' -H 'Content-Type: application/x-www-form-urlencoded'
```

This gives `_4nd_y0u_w1ll_n3v3r_f1nd_m333}`.

“The first part has to be easier,” we thought. “At least this is almost over,” we told ourselves.

I hope you feel our pain sometime.

