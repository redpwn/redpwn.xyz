---
title: Jarvis
author: dwang
date: '2020-05-28'
ctfs:
  - tjctf2020
categories:
  - misc
---

# TJCTF 2020 - Jarvis

Looking at the files we are given, we realized that the challenge is a binary classification task. A simple search reveals [this](https://machinelearningmastery.com/tutorial-first-neural-network-python-keras/) script which we adapted to the `help.csv` dataset.

```python
from numpy import loadtxt
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense

help_dataset = loadtxt("help.csv", delimiter=",")
x = help_dataset[:, 1:10]
y = help_dataset[:, 0]

model = Sequential()
model.add(Dense(12, input_dim=9, activation="relu"))
model.add(Dense(8, activation="relu"))
model.add(Dense(1, activation="sigmoid"))
model.compile(loss="binary_crossentropy", optimizer="adam", metrics=["accuracy"])
model.fit(x, y, epochs=50, batch_size=10)

flag_dataset = loadtxt("flag.csv", delimiter=",")
x = flag_dataset[:, 0:9]

predictions = (model.predict(x) > 0.5).astype("int32")

flag = "".join([str(round(float(prediction))) for prediction in predictions])
print(flag)
```

We get `flag{mnWis_cool}` and guess the correct flag.

```
flag{ml_is_cool}
```
