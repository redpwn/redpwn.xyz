---
title: Hard Shells
author: blevy
date: '2018-09-12'
ctfs:
  - icectf2018
categories:
  - forensics
---

## Points

200

## Description

> After a recent hack, a laptop was seized and subsequently analyzed. The victim of the hack? An innocent mexican restaurant. During the investigation they found this suspicous file. Can you find any evidence that the owner of this laptop is the culprit?

## Solve

```
$ file hardshells
hardshells: Zip archive data, at least v1.0 to extract
$ mv hardshells hardshells.zip
renamed 'hardshells' -> 'hardshells.zip'
```

Apparently the zip file has a password. Let's crack it. We can use the [rockyou](https://github.com/brannondorsey/naive-hashcat/releases/download/data/rockyou.txt) wordlist!

```
$ zip2john hardshells.zip --wordlist=rockyou.txt > john.txt
hardshells.zip->hardshells/ is not encrypted!
ver a  hardshells.zip->hardshells/ is not encrypted, or stored with non-handled compression type
ver 14  efh 5455  efh 7875  hardshells.zip->hardshells/d PKZIP Encr: 2b chk, TS_chk, cmplen=309500, decmplen=5242880, crc=CCF0AB03
$ john --show john.txt 
hardshells.zip:tacos:::::hardshells.zip

1 password hash cracked, 0 left
```

So the password is "tacos" apparently.

```
$ unzip hardshells.zip 
Archive:  hardshells.zip
   creating: hardshells/
[hardshells.zip] hardshells/d password: tacos
  inflating: hardshells/d
```

Let's see what that `d` file is. 🤔

```
$ cd hardshells 
$ file d
d: Minix filesystem, V1, 30 char names, 20 zones
```

It's a Minix filesystem! We can mount it on Linux with a loop device.

```
$ sudo mount -o loop d /mnt
$ cd /mnt
$ ls
dat
```

What could this other file be? 🤔

```
$ file dat
dat: data
$ hexdump -C | head
00000000  89 50 55 47 0d 0a 1a 0a  00 00 00 0d 49 48 44 52  |.PUG........IHDR|
00000010  00 00 07 80 00 00 04 38  08 06 00 00 00 e8 d3 c1  |.......8........|
00000020  43 00 00 20 00 49 44 41  54 78 9c ec dd 79 78 54  |C.. .IDATx...yxT|
00000030  f5 d5 c0 f1 ef 2c d9 26  fb 0a 49 c8 be b0 98 84  |.....,.&..I.....|
00000040  b0 43 d8 77 14 41 b1 8a  8a 76 51 5b 6d 9f da 6a  |.C.w.A...vQ[m..j|
00000050  5b 5b fb 6a ad 7d ad 56  5b fb da 62 a9 5a 5c ea  |[[.j.}.V[..b.Z\.|
00000060  86 2b 6a a1 22 a2 ec b2  25 ec 10 42 02 21 fb be  |.+j."...%..B.!..|
00000070  cf 64 9b 49 32 73 df 3f  62 06 02 01 32 c9 24 93  |.d.I2s.?b...2.$.|
00000080  84 f3 79 9e 3c 30 77 e6  fe e6 64 99 3b 77 ee f9  |..y.<0w...d.;w..|
00000090  9d f3 53 4d 98 30 41 41  08 21 84 10 42 08 21 84  |..SM.0AA.!..B.!.|
```

Because of the `IHDR` and `IDAT`, this looks suspiciously like a PNG file. 🤔

It wasn't detected by `file` because it doesn't have the proper magic byte.

PNG files must begin with the bytes `89 50 4e 47` in order to be valid.

In other words, the "U" in "PUG" needs to be changed to an "N" to make "PNG."

After doing this in a hex editor, we see the following image.

![image](dat_fixed.png)

The flag can be seen typed in the terminal on glitch's glorious Arch Linux rice.
